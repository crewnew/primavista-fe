function mapModule() {
  var map;
  var startingPos = {lat: 58.3765477,lng: 26.718419};
  this.init = function() {
    map = new google.maps.Map(document.getElementById('map'), {
      zoom: 14,
      center: startingPos,
      scrollwheel: false,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      styles: 
        [
        {
            "featureType": "all",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "saturation": 36
                },
                {
                    "color": "#333333"
                },
                {
                    "lightness": 40
                }
            ]
        },
        {
            "featureType": "all",
            "elementType": "labels.text.stroke",
            "stylers": [
                {
                    "visibility": "on"
                },
                {
                    "color": "#ffffff"
                },
                {
                    "lightness": 16
                }
            ]
        },
        {
            "featureType": "all",
            "elementType": "labels.icon",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "administrative",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "color": "#fefefe"
                },
                {
                    "lightness": "20"
                },
                {
                    "saturation": "0"
                }
            ]
        },
        {
            "featureType": "administrative",
            "elementType": "geometry.stroke",
            "stylers": [
                {
                    "color": "#fefefe"
                },
                {
                    "lightness": 17
                },
                {
                    "weight": 1.2
                }
            ]
        },
        {
            "featureType": "landscape",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#eaeaea"
                },
                {
                    "lightness": "10"
                }
            ]
        },
        {
            "featureType": "landscape.natural",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "weight": "1"
                },
                {
                    "lightness": "20"
                }
            ]
        },
        {
            "featureType": "poi",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#f5f5f5"
                },
                {
                    "lightness": "20"
                },
                {
                    "saturation": "0"
                }
            ]
        },
        {
            "featureType": "poi.park",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#c6c6c6"
                },
                {
                    "lightness": "10"
                }
            ]
        },
        {
            "featureType": "road.highway",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "color": "#ffffff"
                },
                {
                    "lightness": "-27"
                }
            ]
        },
        {
            "featureType": "road.highway",
            "elementType": "geometry.stroke",
            "stylers": [
                {
                    "color": "#ffffff"
                },
                {
                    "lightness": 29
                },
                {
                    "weight": 0.2
                }
            ]
        },
        {
            "featureType": "road.arterial",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#ffffff"
                },
                {
                    "lightness": 18
                }
            ]
        },
        {
            "featureType": "road.local",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#ffffff"
                },
                {
                    "lightness": 16
                }
            ]
        },
        {
            "featureType": "transit",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#f2f2f2"
                },
                {
                    "lightness": 19
                }
            ]
        },
        {
            "featureType": "water",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#e9e9e9"
                },
                {
                    "lightness": 17
                }
            ]
        }
    ]
    });    


  }

  this.initMarkers = function() {
    var latlng_pos=[];
    var j=0;
    $('.markers li').each(function(){
      var html = $(this).html();
      var _lon = $(this).data('lon');
      var _lat = $(this).data('lat');
      
      latlng_pos[j]=new google.maps.LatLng(_lat,_lon);
      j++;
      var startingPos = {lat: _lat,lng: _lon};
      var infowindow = new google.maps.InfoWindow({
        content: html
        // maxWidth: 300
      });
      var marker = new google.maps.Marker({
        position: startingPos,
        map: map,
        icon: '../assets/images/marker.png'
      });
       marker.addListener('click', function() {
          infowindow.open(map, marker);
        });
      
    });

    var latlngbounds = new google.maps.LatLngBounds( );
    for ( var i = 0; i < latlng_pos.length; i++ ) {
        latlngbounds.extend( latlng_pos[ i ] );
    }
    map.fitBounds( latlngbounds );    
  }
}

module.exports = mapModule;
