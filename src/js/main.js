window.jQuery = $ = require('jquery');	
// require('./scroll-animatons.js');
// require('./map.js');
require('malihu-custom-scrollbar-plugin')($);
require('theia-sticky-sidebar');


var myMap = require('./map.js');

var googleMap = new myMap();



// var tubular = require('./jquery.tubular.1.0.js');
var YTPlayer = require('../../bower_components/youtubeBackground/src/jquery.youtubebackground.js');
var waypoints = require("../../node_modules/waypoints/lib/jquery.waypoints.js");

window.UI = (function() {
	var activeIndex = '';
    var that = {};


    var initMenu = function() {
		$(".main-menu a").mouseenter(function(){
			if(activeIndex){ $(".menu-backgrounds-container__item:nth-child("+activeIndex+")").removeClass('active');}
		    $('.main-menu').addClass('hover');
		    var color = $(this).data('color');
		    $(this).css('background-color',color);
		    var index = $(this).parent().index() + 1;
		    $(".menu-backgrounds-container__item:nth-child("+index+")").addClass('active');
		    $('.menu-backgrounds-container .header-home__content').show();

		}).mouseleave(function(){
			$('.main-menu').removeClass('hover');
			 $(this).css('background','none');
			 var index = $(this).parent().index() + 1;
			 console.log("mouseout "+ activeIndex);
			 $(".menu-backgrounds-container__item:nth-child("+index+")").removeClass('active');
			 if(activeIndex){ $(".menu-backgrounds-container__item:nth-child("+activeIndex+")").addClass('active');}
		});	
		function checkIfActive() {
			$('.main-menu a').each(function(i, obj) {
	    		if($(this).hasClass('active')) {
	    			activeIndex = $(this).parent().index() + 1;
	    			$(".menu-backgrounds-container__item:nth-child("+activeIndex+")").addClass('active');
	    		}
			});
		}

		$('.nav__toggle').click(function(e){
			e.preventDefault();
			checkIfActive();
			$('body').toggleClass('menu_open');
			$('.menu-container').fadeToggle(200);
		});
		$('.main-menu a').click(function(e){
			checkIfActive();
			$('body').toggleClass('menu_open');
			$('.menu-container').fadeToggle(200);
		});
    }
	
	var initSidebar = function() {
		$('.sticky-cont, .sticky-side').theiaStickySidebar();
	}

	var initScroll = function() {
		$('.scroll_down').click(function(e){
			$('body').animate({ scrollTop: $('.header-home').height()-120}, 600);
		});
	}

	var initPullProgramm = function() { 
		$('.pull-programm').click(function(){
			$(this).addClass('active');
			$('.programm_page ').addClass('active');
			setTimeout(function(){ $('.page_cont').css('display','none'); $('.overflow').css({'overflow':'inherit','position':'static'});}, 350);
		});
		$('.pull-programm').mouseenter(function() {
			$('.page_cont').addClass('pull');
		}).mouseleave(function(){
			$('.page_cont').removeClass('pull');
		});
	}

	var initBgVideo = function() {
		var isMobile = false; //initiate as false
		
		if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) 
	    || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) isMobile = true;
	    
	    if(!isMobile){
			// $('.header-home').tubular({videoId: 'nspepucARoA'});
			$('.header-video').YTPlayer({
			    fitToBackground: true,
			    videoId: 'nspepucARoA',
			    repeat: true
			});		
		}		
	}

	var initModals = function() {
		$('a').click(function(e){
			var target = $(this).data('target');
			if(target && $(target).length){
				e.preventDefault();
				$(target).fadeIn();
				$('body').append('<div class="modal-backdrop"></div>');
				$('body').addClass('overflow');
			}
		});

		$('.modal').on('click', function(e) {
		  if (e.target !== this)
		    return;
		  
		  $(this).fadeOut();
		  $('.modal-backdrop').fadeOut(function(){
		  	$('.modal-backdrop').remove();
		  	$('body').removeClass('overflow');	
		  });
		});
		$('.close-modal').click(function(e){
		 	e.preventDefault();
			$('.modal').fadeOut();
			$('.modal-backdrop').fadeOut(function(){
			  	$('.modal-backdrop').remove();
			  	$('body').removeClass('overflow');	
			});
		});		
	}
			
   
	var initMenuBarScroll = function() {
		$(window).scroll(function() {
	    	if($('.header-home').length){
		        var scroll = $(window).scrollTop(); 
		        var os = $('.header-home').offset().top; 
		        var ht = $('.header-home').height() - 125;
		        if(scroll > os + ht){
		            $(".home-wrapper .main-header").removeClass('main-header--home');
		            $('.home-wrapper .main-header__logo img').attr('src', 'assets/images/logo.svg');

		        }
		        else {
		        	$(".home-wrapper .main-header").addClass('main-header--home');
		        	$('.home-wrapper .main-header__logo img').attr('src', 'assets/images/logo_white.svg');
		        }
	    	}
	    });	
	}

	var initCustomScroller = function() {
		$('.scroll_content').mCustomScrollbar();
	}

	var initScrollAnimations = function() {
		onScrollInit( $('.os-animation') );
		onScrollInit( $('.staggered-animation'), $('.staggered-animation-container') );
	}

	function onScrollInit( items, trigger ) {
	    items.each( function() {
	    var osElement = $(this),
	        osAnimationClass = osElement.attr('data-os-animation'),
	        osAnimationDelay = osElement.attr('data-os-animation-delay');
	      
	        osElement.css({
	            '-webkit-animation-delay':  osAnimationDelay,
	            '-moz-animation-delay':     osAnimationDelay,
	            'animation-delay':          osAnimationDelay
	        });
	        var osTrigger = ( trigger ) ? trigger : osElement;
	        
	        osTrigger.waypoint(function() {
	            osElement.addClass('animated').addClass(osAnimationClass);
	            },{
	                triggerOnce: true,
	                offset: '90%'
	        });
	    });
	}

    

    return {
    	init: function() {
			initMenu();
			initSidebar();
			initScroll();
			initPullProgramm();
			initBgVideo();
			initModals();
			initMenuBarScroll();
			initCustomScroller();
			initScrollAnimations();
			googleMap.init();
			googleMap.initMarkers();
    	},
    	handleMenu: function() {
    		initMenu();
    	},
    	handleSidebar: function() {
    		initSidebar();
    	},
    	handleScroll: function() {
    		initScroll();
    	},
    	handlePullProgramm: function() {
			initPullProgramm();
    	},
    	handleBgVideo: function() {
    		initBgVideo();
    	},
    	handleModals: function(){ 
    		initModals();
    	},
    	handleMenuBarScroll: function() {
    		initMenuBarScroll();
    	},
    	handleCustomScroller: function() {
			initCustomScroller();
    	},
    	handleScrollAnimations: function() {
			initScrollAnimations();
    	},
    	handleInitMap: function() {
			googleMap.init();
    	},
    	handleInitMapMarkers: function() {
    		googleMap.initMarkers();
    	}


    };

})();


$(document).ready(function(){
    
      // UI.init();
	
});


